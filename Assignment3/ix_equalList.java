// The arrays list1 and list2 are identical if they have the same contents. Write a method that
//returns true if list1 and list2 are identical, using the following header:
//public static boolean equals(int[] list1, int[] list2)
//Write a test program that prompts the user to enter two lists of integers and displays whether the
//two are identical. Here are the sample runs. Note that the first number in the input indicates the
//number of the elements in the list. This number is not part of the list.
//Enter list1: 5 2 5 6 6 1
//Enter list2: 5 5 2 6 1 6
//Two lists are identical
//Enter list1: 5 5 5 6 6 1
//Enter list2: 5 2 5 6 1 6
//Two lists are not identical

import java.util.Arrays;
import java.util.Scanner;

public class ix_equalList {

    public static boolean equals(int[] list1, int[] list2){
        if (list1.length != list2.length){
            return false;
        }
        Arrays.sort(list1);
        Arrays.sort(list2);
        return Arrays.equals(list1, list2);
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter list1:  ");
        int l1 = scannedInput.nextInt();
        int[] list1 = new int[l1];
        for (int i = 0; i < l1; i++){
            list1[i] = scannedInput.nextInt();
        }
        System.out.print("Enter list2:  ");
        int l2 = scannedInput.nextInt();
        int[] list2 = new int[l2];
        for (int i = 0; i < l2; i++){
            list2[i] = scannedInput.nextInt();
        }

        if (equals(list1, list2)){
            System.out.println("Two lists are identical");
        }else {
            System.out.println("Two lists are not identical");
        }
    }
}
