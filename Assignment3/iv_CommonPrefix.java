//Write a program that prompts the user to enter two strings and displays the largest common prefix
//of the two strings. Here are some sample runs:
//Enter the first string: Welcome to C++
//Enter the second string: Welcome to programming
//The common prefix is Welcome to
//Enter the first string: Atlanta
//Enter the second string: Macon
//Atlanta and Macon have no common prefix

import java.util.Scanner;

public class iv_CommonPrefix {

    static String findCommonPrefix(String s1, String s2){
        // Using String Builder because we don't know the length of common prefix
        StringBuilder commonPrefix = new StringBuilder();
        int i = 0;
        while (s1.charAt(i) == s2.charAt(i)){
            commonPrefix.append(s1.charAt(i));
            i++;
        }
        return commonPrefix.toString();
    }
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter first string : ");
        String s1 = scannedInput.nextLine();

        System.out.print("Enter second string : ");
        String s2 = scannedInput.nextLine();

        String commonPrefix = findCommonPrefix(s1, s2);
        if (commonPrefix.equals("")){
            System.out.println(s1 + " and " + s2 + " have no common prefix");
        }
        else {
            System.out.println("The common prefix is " + commonPrefix);
        }
    }
}
