import java.util.Scanner;
import java.util.regex.Pattern;

public class v_SSN {

    static boolean isValid(String ssnCode){
        if (ssnCode.length()!= 11){
            return false;
        }

        // Regex
        return Pattern.matches("[0-9]{3}-[0-9]{2}-[0-9]{4}", ssnCode);
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);
        System.out.print("Enter a SSN: ");
        String givenSSN = scannedInput.nextLine().trim();

        if (isValid(givenSSN)){
            System.out.println(givenSSN + " is a valid social security number");
        }else {
            System.out.println(givenSSN + " is an invalid social security number");
        }
    }
}
