// Write a method that counts the number of letters in a string using the following header:
//public static int countLetters(String s)
//Write a test program that prompts the user to enter a string and displays the number of letters in the string.

import java.util.Scanner;

public class i_countLetters {
    public static int countLetters(String s){
        int sum = 0;
        for (int i = 0; i < s.length(); i++){
            if (Character.isLetter(s.charAt(i))){
                sum++;
            }
        }
        return sum;
    }
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);
        System.out.print("Enter string : ");
        String givenString = scannedInput.nextLine();
        System.out.println("Letters in given string is : " + countLetters(givenString));
    }
}