import java.util.Scanner;

public class vii_ConsecutiveFour {

    public static boolean isConsecutiveFour(int[] values){
        int len = values.length;
        for (int i = 0; i < len-3; i++){
            if (values[i] == values[i+1] && values[i] == values[i+2] && values[i] == values[i+3]){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);
        System.out.print("Enter the number of values:  ");
        int length = scannedInput.nextInt();

        System.out.print("Enter the values:  ");
        int[] givenArray = new int[length];
        for (int i = 0; i < length; i++){
            givenArray[i] = scannedInput.nextInt();
        }

        if (isConsecutiveFour(givenArray)){
            System.out.println("The list has consecutive fours");
        }else {
            System.out.println("The list has no consecutive fours");
        }
    }
}
