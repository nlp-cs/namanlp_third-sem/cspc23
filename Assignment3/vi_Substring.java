import java.util.Scanner;

public class vi_Substring {

    // Calculate Hash based on ASCII value of Character
    static int findHash(String givenString){
        int hash = 0;

        for (int i = 0; i < givenString.length(); i++){
            hash+= givenString.charAt(i);
        }
        return hash;
    }

    static boolean isSubString(String superString, String subString){

        if (superString.length() < subString.length()){
            return false;
        }
        int superLength = superString.length();
        int subLength = subString.length();

        int currentHash = findHash(superString.substring(0, subLength));
        int toFindHash = findHash(subString);

        for (int i = 0; i <= superLength- subLength; i++){
            if (currentHash == toFindHash){
                int found = 1;
                for (int j = 0; j < subLength; j++){
                    if (superString.charAt(i+j) != subString.charAt(j)){
                        found = 0; break;
                    }
                }
                if (found == 1){
                    return true;
                }
            }

            // reduce current hash of current starting character
            currentHash-= superString.charAt(i);
            // Increase only if given statement, else we have reached end of text.
            if (i < superLength-subLength){
                currentHash += superString.charAt(i + subLength);
            }
        }

        return false;
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter string s1: ");
        String s1 = scannedInput.nextLine();

        System.out.print("Enter string s2: ");
        String s2 = scannedInput.nextLine();

        if ( isSubString(s1, s2) ){
            System.out.println(s2 + " is a substring of " + s1);
        }
        else {
            System.out.println(s2 + " is not a substring of " + s1);
        }
    }
}
