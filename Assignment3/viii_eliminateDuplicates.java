// Write a method that returns a new array by eliminating the duplicate values in the array using the
//following method header:
//public static int[] eliminateDuplicates(int[] list)
//Write a test program that reads in ten integers, invokes the method, and displays the result. Here is
//the sample run of the program:
//Enter ten numbers: 1 2 3 2 1 6 3 4 5 2
//The distinct numbers are: 1 2 3 6 4 5

import java.util.Arrays;
import java.util.Scanner;

public class viii_eliminateDuplicates {

    static boolean searchArray(int [] list, int key){
        for (int e : list){
            if (e==key){
                return true;
            }
        }
        return false;
    }

    public static int[] eliminateDuplicates(int[] list){
        int[] list2 = new int[list.length];
        int top = 0;
        for (int e : list){
            if (!searchArray(list2, e)){
                list2[top] = e;
                top++;
            }
        }
        // Create a new list of appropriate storage, and copy elements to it.
        // Length of new list is top, not top + 1 because top is already incremented after adding element
        int[] list3 = new int[top];

        System.arraycopy(list2, 0, list3, 0, top);
        return list3;
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter ten numbers: ");
        int[] givenArray = new int[10];
        for (int i = 0; i < 10; i++){
            givenArray[i] = scannedInput.nextInt();
        }

        int[] newArray = eliminateDuplicates(givenArray);
        System.out.println("The distinct numbers are: " + Arrays.toString(newArray));
    }
}
