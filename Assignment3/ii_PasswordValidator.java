// Some websites impose certain rules for passwords. Write a method that checks whether a string is a valid password. Suppose the password rules are as follows:
//■ A password must have at least eight characters.
//■ A password consists of only letters and digits.
//■ A password must contain at least two digits.
//Write a program that prompts the user to enter a password and displays Valid Password if the rules are followed or Invalid Password otherwise.

import java.util.Scanner;

public class ii_PasswordValidator {
    static boolean PasswordValidator(String givenPassword){

        // This function will return 0 if any of given conditions fails, else it will return 1

        if (givenPassword.length() < 8){
            return false;
        }
        int countDigit = 0;
        for (int i = 0; i < givenPassword.length(); i++){
            if ( !Character.isLetter(givenPassword.charAt(i) ) && !Character.isDigit(givenPassword.charAt(i) )){
                return false;
            }
            if ( Character.isDigit( givenPassword.charAt(i) )){
                countDigit++;
            }
        }

        // Returns true if password has at least 2 digits, else false
        return countDigit >= 2;
    }

    // ================================================================ Magic Starts Here ==================================================================
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);
        System.out.print("Enter password : ");
        String password = scannedInput.nextLine();

        if (PasswordValidator(password)){
            System.out.println("Valid Password");
        } else {
            System.out.println("Invalid Password");
        }
    }
}
