import java.util.Scanner;

public class xv_StopWatch {

    static class StopWatch{
        Long startTime, endTime;

        void start(){
            startTime = System.currentTimeMillis();
        }
        void stop(){
            endTime = System.currentTimeMillis();
        }

        long elapsedTime(){
            return endTime- startTime;
        }

        StopWatch(){
            start();
            endTime = startTime;
        }
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        int choice = 0;

        StopWatch stopWatch1 = new StopWatch();
        while (choice!=4){
            System.out.print("Enter choice \n1. -> Start Watch \n2. -> Stop Watch\n3. -> Find Difference between Start and end\n4. -> Quit\n ");
            choice = scannedInput.nextInt();
            switch (choice) {
                case 1 -> stopWatch1.start();
                case 2 -> stopWatch1.stop();
                case 3 -> System.out.println("Elapsed time = " + stopWatch1.elapsedTime());
                case 4 -> System.out.println("Thank You, have a good day!");
                default -> System.out.println("Sorry, wrong choice!");
            }
        }
    }
}
