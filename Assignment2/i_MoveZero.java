//Write a Java program that reads an array of integers and then moves every zero to the right side i.e. towards the end.

import java.util.Scanner;

public class i_MoveZero {

    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Enter length of array : ");
        int length = scannedInput.nextInt();

        int[] givenArray = new int[length];

        System.out.println("Enter Array : ");
        for (int i = 0; i < length; i++){
            givenArray[i] = scannedInput.nextInt();
        }

        for (int i = length-1; i >= 0; i--){
            if (givenArray[i] == 0){
                int j = i;
                while (j < length-1 && givenArray[j+1] != 0){
                    givenArray[j] = givenArray[j+1];
                    givenArray[j+1] = 0;
                    j++;
                }
            }
        }

        for (int i = 0; i < length; i++){
            System.out.print(givenArray[i] + " ");
        }
    }
}
