public class xiv_SavingsAccount {

    static class SavingsAccount{
        static double annualInterestRate = 0.087;
        double savingsBalance = 0.00;
        double calculateMonthlyInterest(){
            double interest = annualInterestRate*savingsBalance/12;
            savingsBalance+=interest;
            return interest;
        }

        static void modifyInterestRate(double interest){
            annualInterestRate = interest;
        }
    }

    public static void main(String[] args) {
        SavingsAccount saver1 = new SavingsAccount();
        saver1.savingsBalance = 2000;
        SavingsAccount saver2 = new SavingsAccount();
        saver2.savingsBalance = 3000;

        SavingsAccount.modifyInterestRate(4.00);
        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();

        System.out.println("Saver 1 balance : " + saver1.savingsBalance);
        System.out.println("Saver 2 balance : " + saver2.savingsBalance);
        SavingsAccount.modifyInterestRate(5.00);
        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();

        System.out.println("Saver 1 balance : " + saver1.savingsBalance);
        System.out.println("Saver 2 balance : " + saver2.savingsBalance);

    }
}
