import java.util.Scanner;

public class ix_TrimString {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter string : ");
        String givenString = scannedInput.nextLine();
        System.out.println("String with whitespaces : \"" + givenString + "\"");
        givenString = givenString.trim();
        System.out.println("After trimming, string is : \"" + givenString + "\"");
    }
}
