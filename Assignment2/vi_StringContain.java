import java.util.Scanner;

public class vi_StringContain {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter first string : ");
        String firstString = scannedInput.nextLine().trim();
        System.out.print("Enter second string : ");
        String secondString = scannedInput.nextLine().trim();

        if (secondString.contains(firstString)){
            System.out.println("Yes, second string contains first string");
        }else {
            System.out.println("No, second string does not contain first string");
        }
    }
}
