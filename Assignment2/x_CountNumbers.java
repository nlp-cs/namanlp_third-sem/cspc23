import java.util.Scanner;

public class x_CountNumbers {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        int[] countArray = new int[50];

        System.out.println("Enter the numbers : ");
        int x = 0;

        while (x!=-1){
            x = scannedInput.nextInt();

            // Print Array if -1 is entered
            // Print counting of (i+1) * 2 index, for example, if i = 0, it is frequency of 2. If i = 10, it is frequency of 22.
            if (x==-1){
                for (int i = 0 ; i < 50; i++){
                    if (countArray[i] != 0)
                        System.out.println(2*(i+1) + " occurs " + countArray[i] + " times");
                }
            }

            // Skip the number if out of bound
            if (x <2 || x > 100)
                continue;

            // Increment (x/2) -1 th index of array . For example, if we enter 2, index 0 is incremented. For 50, index 24 is incremented
            if (x%2 == 0)
                countArray[ (x/2) -1]++;
        }
    }
}
