import java.util.Scanner;

public class vii_Compare {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter first string : ");
        String firstString = scannedInput.nextLine().trim();
        System.out.print("Enter second string : ");
        String secondString = scannedInput.nextLine().trim();

        int comparison = secondString.compareTo(firstString);
        if (comparison == 0){
            System.out.println("Both strings are same");
        } else if (comparison < 0) {
            System.out.println("String 2 comes first lexicographically");
        }else {
            System.out.println("String 1 comes first lexicographically");
        }
    }
}
