import java.util.Scanner;

public class xii_SixTracker {

    public static class NumberOfSixes{
        int sixesHit;

        NumberOfSixes(){
            sixesHit = 0;
        }
        int getSixesHit(){
            return sixesHit;
        }
        void setSixesHit(){
            sixesHit++;
        }
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        int choice = 0;
        NumberOfSixes sixTracker = new NumberOfSixes();
        while (choice!=3){
            System.out.print("Enter choice \n1. -> Increase six by 1\n2. -> Print the number of sixes\n3. -> Quit\n ");
            choice = scannedInput.nextInt();
            switch (choice) {
                case 1 -> sixTracker.setSixesHit();
                case 2 -> System.out.println("Sixes hit by now are : " + sixTracker.getSixesHit());
                case 3 -> System.out.println("Thank You, have a good day!");
                default -> System.out.println("Sorry, wrong choice!");
            }
        }
    }
}
