import java.util.Scanner;

public class v_FindDivisible {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter x, y and p : ");

        int x = scannedInput.nextInt();
        int y = scannedInput.nextInt();
        int p = scannedInput.nextInt();

        System.out.print("\nNumbers in range x and y, divisible by p are : ");

        for (int i = x; i < y; i++){
            if ( i % p == 0){
                System.out.print(i + " ");
            }
        }
    }
}
