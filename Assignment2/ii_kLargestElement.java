import java.util.Scanner;
import java.util.Arrays;

public class ii_kLargestElement {

    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Enter total length of array : ");
        int length = scannedInput.nextInt();

        int[] givenArray = new int[length];

        System.out.println("Enter Array : ");
        for (int i = 0; i < length; i++){
            givenArray[i] = scannedInput.nextInt();
        }

        System.out.println("Enter the value of k : ");
        int k = scannedInput.nextInt();

        if (k > length || k<=0){
            System.out.println("ERROR : k must be greater than 0 and less than length of array");
            return ;
        }

        int[] largeElements = new int[k];
        for (int i = 0; i < length; i++){
            if (largeElements[0] < givenArray[i]){
                largeElements[0] = givenArray[i];
                Arrays.sort(largeElements);
            }
        }
        System.out.println("K largest element is : " + largeElements[0]);

    }
}
