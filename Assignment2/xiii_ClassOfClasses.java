import java.util.Scanner;

class ReadingMaterial{
    String typeOfMaterial;

    int pages;
    int characters;
    String[] charArray;


    void takeCharacters(){
        System.out.println("\nEnter " + characters + " characters : ");
        Scanner scannedInput = new Scanner(System.in);
        for (int i = 0; i<characters; i++){
            charArray[i] = scannedInput.next();
        }
    }
    // Constructor of class
    ReadingMaterial(String type, int numberOfCharacters, int page){
        typeOfMaterial = type;
        characters = numberOfCharacters;
        charArray = new String[numberOfCharacters];
        takeCharacters();
        pages = page;
    }

    void printData(){
        System.out.println("Number of pages : " + pages);
        System.out.print("Characters are : ");
        for (String character : charArray){
            System.out.print(character + " ");
        }
    }
}

public class xiii_ClassOfClasses {
    public static void main(String[] args) {
        ReadingMaterial book = new ReadingMaterial("Book", 2, 200);
        book.printData();
        ReadingMaterial novel = new ReadingMaterial("Novel", 3, 748);
        novel.printData();

    }
}
