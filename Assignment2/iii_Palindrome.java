import java.util.Scanner;

public class iii_Palindrome {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter positive integer : ");
        int number = scannedInput.nextInt();

        if (number <= 0){
            System.out.println("ERROR : Number not positive number!");
            return;
        }

        int reverseNumber = 0, temp = number;

        while (temp > 0){
            reverseNumber = ( reverseNumber * 10 ) + (temp%10);
            temp/=10;
        }

        if (number == reverseNumber){
            System.out.println("Yes, " + number + " is a palindrome !");
        }
        else {
            System.out.println("No, " + number + " is not a palindrome !");
        }
    }
}
