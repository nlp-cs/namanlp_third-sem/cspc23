import java.util.Scanner;

public class iv_ThreeIncreasingIntegers {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter length of Array : ");
        int length = scannedInput.nextInt();
        int[] givenArray = new int[length];
        for (int i = 0; i < length; i++){
            givenArray[i] = scannedInput.nextInt();
        }

        for (int i = 0; i < length-2; i++){
            if (givenArray[i] < givenArray[i+1] && givenArray[i+1] < givenArray[i+2]){
                System.out.println("Yes, given array contains 3 consecutive adjacent numbers : " + givenArray[i] + ", " + givenArray[i+1] + ", " + givenArray[i+2]);
                return;
            }
        }

        System.out.println("No,  given array does not contain 3 consecutive adjacent numbers.");
    }
}
