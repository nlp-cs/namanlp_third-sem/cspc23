import java.util.Scanner;

public class xi_1DArray {

    static int findInArray(int[] array, int key){
        for (int j : array)
            if (j == key)
                return 1;
        return 0;
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        int length = 0;
        int[] enteredArray = new int[5];

        while (length < 5){
            int x = scannedInput.nextInt();
            if ( x >= 10 && x <= 100){
                if (findInArray(enteredArray, x) == 0){
                    enteredArray[length] = x;
                    length++;
                    System.out.println(x);
                }
            }
            for (int i : enteredArray){
                if (i==0)
                    break;
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
