import java.util.Scanner;

public class viii_RabinKarp {

    // Calculate Hash based on ASCII value of Character
    static int findHash(String givenString){
        int hash = 0;

        for (int i = 0; i < givenString.length(); i++){
            hash+= givenString.charAt(i);
        }
        return hash;
    }

    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);
        System.out.print("Enter text to search in : ");
        String text = scannedInput.nextLine();
        int textLength = text.length();

        System.out.print("Enter string to search for : ");
        String toFind = scannedInput.nextLine();
        int toFindLength = toFind.length();

        if (textLength < toFindLength){
            System.out.println("String can not be searched because it is longer than text itself!");
            return;
        }

        // Find Hash of first n letters of both strings
        int currentHash = findHash(text.substring(0, toFindLength));
        int toFindHash = findHash(toFind);

        for (int i = 0; i <= textLength-toFindLength; i++){
            if (currentHash == toFindHash){
                int found = 1;
                for (int j = 0; j < toFindLength; j++){
                    if (text.charAt(i+j) != toFind.charAt(j)){
                        found = 0;break;
                    }
                }
                // If found at first character itself, prints 1
                if (found == 1){
                    System.out.println("Text contains string at : " + (i+1));
                }
            }

            // reduce current hash of current starting character
            currentHash-= text.charAt(i);
            // Increase only if given statement, else we have reached end of text.
            if (i < textLength-toFindLength){
                currentHash += text.charAt(toFindLength+i);
            }
            else {
                System.out.println("DONE!");
            }
        }
    }
}
