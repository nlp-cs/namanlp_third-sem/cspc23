public class InterfacePractice {
    public interface vehicle{
//        void printMethod();
    }

    public static class car implements vehicle {
        int tyres;
        String name;
        String brand;
        double price;
        // Constructor
        car(){
            tyres = 4;
        }

        car(String name, String brand, double price){
            this.tyres = 4;
            this.name = name;
            this.brand = brand;
            this.price = price;
        }

        public void printMethod() {
            System.out.println("Car " + name + " has " + tyres + " tyres and is manufactured by " + brand + " and costs $"+ price + " only");
        }

        public static void main(String[] args) {
            AbstractClassPractice.car car1 = new AbstractClassPractice.car("I-Pace", "Jaguar", 150_000.00);
            car1.printMethod();
        }
    }
    }

}

