public class AbstractClassPractice {
    public static abstract class vehicle{
        int tyres;
        String name;
        String brand;
        double price;
        public abstract void printMethod();
        vehicle(){
            tyres = 2;
            name = "";
            brand = "";
            price = 0.0;
        }
    }

    public static class car extends vehicle{
        // Constructor
        car(){
            super();
            tyres = 4;
        }

        car(String name, String brand, double price){
            this.tyres = 4;
            this.name = name;
            this.brand = brand;
            this.price = price;
        }

        @Override
        public void printMethod() {
            System.out.println("Car " + name + " has " + tyres + " tyres and is manufactured by " + brand + " and costs $"+ price + " only");
        }
    }

    public static void main(String[] args) {
        car car1 = new car("I-Pace", "Jaguar", 150_000.00);
        car1.printMethod();
    }
}
