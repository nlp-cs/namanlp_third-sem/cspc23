import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;

public class fileHandlingPractice {
    public static void main(String[] args) {
        System.out.println("Hello");
        // Basic File Functionality
        File givenFile = new File("sampleData.txt");

        if (givenFile.exists()){
            if (givenFile.isFile()){
                System.out.println("Given file exists in file system and is " + givenFile.length() + " bytes length");
            }else if (givenFile.isDirectory()){
                System.out.println("Given file is not a file but a directory!");
            }
            System.out.println("Absolute path of file is : " + givenFile.getAbsolutePath());
            System.out.println("It was last modified on : " + new Date(givenFile.lastModified()));
            if (givenFile.canRead() && givenFile.canWrite()){
                System.out.println("Given file can be read and written !");
            } else {
                System.out.println("Not enough functionalities available for file...");
            }
            if (givenFile.canExecute()){
                System.out.println("Given file can be executed !");
            } else{
                System.out.println("Sorry, given file can not be executed !");
            }

            // Reading from file
            try {
                System.out.println("\n+------------------------------------------------------------------------------------------------------------------------------------------------+");
                System.out.println("Contents of File");
                Scanner scannedFile = new Scanner(givenFile);
                while (scannedFile.hasNext()){
                    System.out.println(scannedFile.nextLine());
                }
                System.out.println("+------------------------------------------------------------------------------------------------------------------------------------------------+\n");

                // Closing file

                scannedFile.close();
            } catch (
                    FileNotFoundException e) {
                throw new RuntimeException(e);
            }


        }else {
            System.out.println("Given path does not exist !");
        }

    }
}
