#include <stdio.h>


void swap(int * a, int * b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void maxHeapify(int array[], int length, int root){
    int left = 2*root+1;
    int right = 2*root+2;
    int max = root;

    if ( left < length && array[root] < array[left]){
        max = left;
    }
    if ( right < length && array[max] < array[right]){
        max = right;
    }

    if ( max != root){
        swap(&array[max], &array[root]);
        maxHeapify(array, length, max);
    }

}

int main(){/*
    int arr[] = { 12, 11, 13, 5, 6, 7 };
    maxHeapify(arr, 6, 5);
    maxHeapify(arr, 6, 4);
    maxHeapify(arr, 6, 3);
    maxHeapify(arr, 6, 2);
    maxHeapify(arr, 6, 1);
    maxHeapify(arr, 6, 0);

    printf("%d %d %d %d %d %d ", arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]);*/
}
