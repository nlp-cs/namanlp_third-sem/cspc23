import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PrintStackTrace {
    public static void main(String[] args) {

        Scanner scannedInput = new Scanner(System.in);
        while (true){

            try{

                System.out.print("Enter String : ");
                String givenString = scannedInput.nextLine();
                System.out.println("5th Letter of string is : " + givenString.charAt(4));
                break;
            }catch (InputMismatchException except){
                System.out.println("Sorry, " + Arrays.toString(except.getStackTrace()));
            }
            catch (IndexOutOfBoundsException except){
                System.out.println("Sorry, Stack trace : " + Arrays.toString(except.getStackTrace()));
            }
            finally {
                System.out.println("Done");
            }
            System.out.println("Hello");
        }
    }
}
