public class MethodOverride {

    public static class baseClass{
        public int var1;
        public void setVar1(int givenNum){
            var1 = givenNum;
        }
        public int getVar1(){
            return var1;
        }

    }

    public static class childClass extends baseClass{

        public int var2;

        public void setVar1(int givenNum){
            var1 = givenNum;
            var2 = givenNum;
        }
        public int getVar1(){
            System.out.println("Variable is : " + var1);
            return var1;
        }

    }

    public static void main(String[] args) {
        childClass object1 = new childClass();

        object1.setVar1(20);
        System.out.println(object1.getVar1());
    }

}
