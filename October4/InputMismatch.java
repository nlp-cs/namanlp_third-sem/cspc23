import java.util.InputMismatchException;
import java.util.Scanner;

public class InputMismatch {
    public static void main(String[] args) {
        System.out.print("Enter Integer to divide  100 :  ");
        Scanner scannedInput = new Scanner(System.in);
        do
        {
            try {
                int number = scannedInput.nextInt();
                System.out.println("100 / " + number + " = " + 100.0/(double) number);
                break;
            }
            catch (InputMismatchException except){
                System.out.println("Sorry, enter a number, try again. ");
                scannedInput.nextLine();
            }
            catch (ArithmeticException except){
                System.out.println("Exception is : " + except);
            }
            finally {
                System.out.println("\nExecuted Successfully");
            }
        }while (true);
    }
}
