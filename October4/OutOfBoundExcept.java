import java.util.InputMismatchException;
import java.util.Scanner;

public class OutOfBoundExcept {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        while (true){

            try{
                System.out.print("Enter String : ");
                String givenString = scannedInput.nextLine();
                System.out.println("5th Letter of string is : " + givenString.charAt(4));
                break;
            }catch (InputMismatchException except){
                System.out.println("Sorry, wrong input pattern");
            }
            catch (IndexOutOfBoundsException except){
                System.out.println("Sorry, String too short");
            }
            finally {
                System.out.println("Execution Done");
            }

        }
    }
}
