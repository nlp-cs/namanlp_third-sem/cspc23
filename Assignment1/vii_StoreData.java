import java.util.Scanner;

public class vii_StoreData {

    public static class individualData{
        String firstName;
        String lastName;
        long phoneNumber;      // Because integer can not take all 10-digit numbers

        public void takeInput(int serialNumber){
            System.out.println("Enter data for individual number : " + serialNumber);
            Scanner scannedInput = new Scanner(System.in);

            firstName = scannedInput.next();
            lastName = scannedInput.next();
            phoneNumber = scannedInput.nextLong();
        }

        public void display(int serialNumber){
            System.out.println("Data for individual number "+ serialNumber + " is ");
            System.out.println("\tFirst Name : " + firstName + "\t\t\t Last Name : " + lastName + "\t\t\t Phone Number : " + phoneNumber);
        }
    }

    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);
        System.out.println("Enter number of individuals : ");
        int totalIndividuals = scannedInput.nextInt();

        individualData[] dataCollection = new individualData[totalIndividuals];

        for (int i = 0; i < totalIndividuals; i++){
            dataCollection[i] = new individualData();
            dataCollection[i].takeInput(i+1);
        }

        System.out.println("===============================================================================================");

        for (int i = 0; i < totalIndividuals; i++){
            System.out.println();
            dataCollection[i].display(i+1);
        }

    }
}
