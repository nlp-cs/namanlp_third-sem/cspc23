import java.util.Scanner;

public class iv_Interest {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Enter Balance : ");
        double balance = scannedInput.nextDouble();
        System.out.println("Enter annual interest rate : ");
        double annualRate = scannedInput.nextDouble();

        double interest = (balance * annualRate ) / 1200.0;
        System.out.println("Monthly installment is : " + interest);
    }
}
