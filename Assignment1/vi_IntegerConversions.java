import java.util.Scanner;

public class vi_IntegerConversions {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);
        System.out.println("Enter integer : ");
        int a = scannedInput.nextInt();
        System.out.println("Integer " + a + " is represented by " + Integer.toBinaryString(a) + " in binary, " + Integer.toOctalString(a) + " in octal and " + Integer.toHexString(a) + " in Hexadecimal");
    }
}
