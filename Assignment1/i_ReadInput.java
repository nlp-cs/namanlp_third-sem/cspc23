import java.util.Scanner;

public class i_ReadInput {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter integer between 0 and 1000 : ");
        int num = myObj.nextInt();
        int product = 1;
        while (num > 1){
            product *= num % 10;
            num/=10;
        }
        System.out.println("Product of digits is : " + product);
    }
}
