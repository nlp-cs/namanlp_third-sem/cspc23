import java.util.Scanner;

public class iii_BMI {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);

        System.out.println("Enter Height in inches : ");
        double height = myObj.nextDouble();
        if (height == 0.0D){
            System.out.println("Sorry, height can not be zero");
        }
        height *= 0.0254;       // Converting to meters

        System.out.println("Enter weight in pounds : ");
        double weight = myObj.nextDouble();
        weight *= 0.45359237;    // Converting to pounds

        double BMI = weight / (height * height);
        System.out.println("BMI is " + BMI);
    }
}
