import java.util.Scanner;

public class v_Factor {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Enter the two integers : ");
        int a = scannedInput.nextInt();
        int b = scannedInput.nextInt();

        if ( (a>=b && a%b == 0) || (b>a && b%a == 0) ){
            System.out.println("Yes, it is a multiple");
        }
        else {
            System.out.println("No, not a multiple");
        }
    }
}
