import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeSet;

public class viii_TreeSet {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter the text input : ");
        String [] givenStrings = scannedInput.nextLine().split(" ");

        // asList inserts all the elements from Arrays to any collection in O(1) time
        TreeSet<String> givenTS = new TreeSet<>(Arrays.asList(givenStrings));

        System.out.println("\nOur tree set is : " + givenTS);
    }
}
