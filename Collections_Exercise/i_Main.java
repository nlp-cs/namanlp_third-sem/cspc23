public class i_Main {
    public static void main(String[] args) {
        i_MyList<Double> myList = new i_MyList<>();

        myList.add(0,2.0);
        myList.add(0,5.4);
        myList.add(0,10.3);
        myList.add(2, 21.2);
        myList.add(2, 21.2);
        myList.add(2, 21.2);
        myList.add(3, 123.4);
        myList.add(2, 24.2);

//        myList.remove(0);

        System.out.println("Size of data is : " + myList.size());

        for (int i = 0; i < myList.size(); i++){
            System.out.println("Data at " + i + " is : " + myList.get(i));
        }

        System.out.println("\n\nFirst Index of 21.2 is " + myList.indexOf(21.2) );
        System.out.println("Last Index of 21.2 is " + myList.lastIndexOf(21.2) );


    }
}
