// Use a HashMap to create a reusable class for choosing one of the 13
//predefined colors in class Color. The names of the colors should be used as
//keys, and the predefined Color objects should be used as values. Place this
//class in a package that can be imported into any Java program. Use your new
//class in an application that allows the user to select a color and draw a shape
//in that color.

import java.awt.*;
import java.util.HashMap;

public class iv_Color {
    public static HashMap<String, Color> ColorMap = new HashMap<>();

    public static void setColorMap() {

        ColorMap.put("black", Color.black);
        ColorMap.put("blue", Color.blue);
        ColorMap.put("cyan", Color.cyan);
        ColorMap.put("darkgray", Color.darkGray);
        ColorMap.put("gray", Color.gray);
        ColorMap.put("green", Color.green);
        ColorMap.put("lightgray", Color.lightGray);
        ColorMap.put("magenta", Color.magenta);
        ColorMap.put("orange", Color.orange);
        ColorMap.put("pink", Color.pink);
        ColorMap.put("red", Color.red);
        ColorMap.put("white", Color.white);
        ColorMap.put("yellow", Color.yellow);
    }
}
