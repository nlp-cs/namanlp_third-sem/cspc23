import java.util.HashMap;
import java.util.Scanner;

public class v_FindDuplicate {
    public static void main(String[] args) {
        HashMap<String, Integer> freq = new HashMap<>();

        System.out.print("Enter string : ");
        Scanner scannedInput = new Scanner(System.in);

        // We are first replacing all punctuation marks in string with ' ' and then splitting it over ' '
        String [] givenS = scannedInput.nextLine().toLowerCase().replaceAll("\\p{Punct}","").split(" ");

        for (String s : givenS){
            if (freq.containsKey(s)){
                freq.put(s, freq.get(s) + 1);
            }
            else{
                freq.put(s, 1);
            }
        }

        System.out.println("Duplicated words are");
        for(String s : freq.keySet()){
            if (freq.get(s) >= 2){
                System.out.println("\"" +s + "\" appears " + freq.get(s));
            }
        }
    }
}
