import java.util.Collection;

public interface MyList<E> extends Collection<E> {
    void add(int index,  E element);
    E get(int index);
    int indexOf(E object);
    int lastIndexOf(E e);
    E remove(int index);
    E set(int index, E e);
}
