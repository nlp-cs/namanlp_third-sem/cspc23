import java.util.HashMap;
import java.util.Scanner;

public class iii_CountFreq {
    public static void main(String[] args) {
        HashMap<Character, Integer> freq = new HashMap<>();

        System.out.print("Enter string : ");
        Scanner scannedInput = new Scanner(System.in);

        char [] givenS = scannedInput.nextLine().toCharArray();
        for (char ch : givenS){
            if (freq.containsKey(ch)){
                freq.put(ch, freq.get(ch) + 1);
            }
            else{
                freq.put(ch, 1);
            }
        }

        for(Character ch : freq.keySet()){
            System.out.println("\"" +ch + "\" appears " + freq.get(ch));
        }
    }
}
