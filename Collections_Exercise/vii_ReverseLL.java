import java.util.LinkedList;
import java.util.Scanner;

public class vii_ReverseLL {
    public static void main(String[] args) {
        Scanner scannedInput = new Scanner(System.in);

        System.out.print("Enter 10 characters : ");
        String givenCh = scannedInput.nextLine();

        if (givenCh.length() < 10){
            System.out.println("Sorry, length should be at least 10 characters");
            return;
        }

        LinkedList<Character> myLL = new LinkedList<>();

        for (int i = 0; i < 10; i++){
            myLL.add(givenCh.charAt(i));
        }

        // Second Linked list
        LinkedList<Character> secondLL = new LinkedList<>();

        // Adding later elements at first position, that is reversing the linked list
        for (int i = 0; i < 10; i++){
            secondLL.add(0, myLL.get(i));
        }

        System.out.print("\nFirst Linked List : ");
        for (Character ch : myLL){
            System.out.print( " " + ch + " ");
        }

        System.out.print("\nSecond Linked List : ");
        for (Character ch : secondLL){
            System.out.print( " " + ch + " ");
        }
    }
}
