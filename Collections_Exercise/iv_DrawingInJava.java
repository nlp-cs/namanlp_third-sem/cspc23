import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class iv_DrawingInJava {
    public static void main(String[] args) {
        iv_Color.setColorMap();

        System.out.print("Enter color : ");
        Scanner scannedInput = new Scanner(System.in);
        String selectedColor = scannedInput.next().toLowerCase();

        // If color not found
        if (!iv_Color.ColorMap.containsKey(selectedColor)){
            System.out.println("Sorry color doesn't exist");
            return;
        }

        Color givenC = iv_Color.ColorMap.get(selectedColor);

        // Make Frame
        JFrame frame = new JFrame("Drawing the shape with " + selectedColor);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000,1000);
        frame.setVisible(true);

        // Make shape
        int height = 200, width = 100;
        frame.getContentPane().add(new MyComponent(height, width, givenC));

    }
}

class MyComponent extends JComponent {
    int height;
    int width;
    Color givenC;

    MyComponent(int h, int w, Color c){
        height = h;
        width = w;
        givenC = c;
    }

    public void paint(Graphics g) {
        g.setColor(givenC);
        g.drawOval(250, 20, height, width);

        // Draw and fill rectangle at 50, 50 of height, width dimensions
        g.drawRect(50, 50, width, height);
        g.fillRect(50, 50, width, height);
//        g.draw3DRect(100, 100, width, height, false);
        g.draw3DRect(100, 100, width, height, false);

    }
}
