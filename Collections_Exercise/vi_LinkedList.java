import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class vi_LinkedList {
    public static void main(String[] args) {
        LinkedList<Integer> myLL = new LinkedList<>();
        Random rand = new Random();

        for (int i = 0; i < 25; i++){
            myLL.add(1+rand.nextInt(99));
        }

        System.out.print("Linked list before sorting : ");
        for (int i = 0; i < 25; i++){
            System.out.print(" " + myLL.get(i));
        }
        System.out.print("\n\nLinked list after sorting : ");

        int sum = 0;
        Collections.sort(myLL);
        for (int i = 0; i < 25; i++){
            System.out.print(" " + myLL.get(i));
            sum+=myLL.get(i);
        }

        System.out.println("\n\nSum of elements is : " + sum);
        System.out.println("Average of elements is : " + (float)sum/ 25.0F);

    }
}
