import java.util.*;

public class ix_PriorityQueue {
    public static void main(String[] args) {
        PriorityQueue<Double> pQueue = new PriorityQueue<>(Collections.reverseOrder());

        int choice = 0;

        Scanner scannedInput  = new Scanner(System.in);

        while (choice!=3){
            System.out.println("Enter choice, \n1->Enter New Double\n2->Print the Priority Queue\n3->Exit");
            choice = scannedInput.nextInt();

            switch (choice) {
                case 1 -> pQueue.add(scannedInput.nextDouble());
                case 2 -> System.out.println("The priority queue is : " + pQueue);
                case 3 -> System.out.println("Have a good day !");
                default -> System.out.println("Sorry, wrong choice. Try again");
            }
        }

    }
}
