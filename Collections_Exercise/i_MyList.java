import java.util.Collection;
import java.util.Iterator;

public class i_MyList<E> implements MyList<E>{

    // We are taking array of max capacity 10_000 elements


    private final int capacity = 10_000;
    private int filled = 0;
    private final E[] data = (E[]) ( new Object[capacity]);

    @Override
    public void add(int index, E Element){

        if (index < 0){
            System.out.println("Sorry, negative indices not supported !");
        }

        if (filled >= capacity){
            System.out.println("Array full !");
            return;
        }
        if (index > filled){
            filled = index+1;
            data[index] = Element;
            return;
        }
        for (int i = filled; i > index && i > 0; i--){
            data[i] = data[i-1];
        }
        data[index] = Element;
        filled++;
    }

    @Override
    public E get(int index) {
        if (index >= filled || index < 0){
            System.out.println("Sorry, index out of bound");
            return null;
        }
        else return data[index];
    }

    @Override
    public int indexOf(E object) {
        for (int i = 0; i < filled; i++){
            if (data[i].equals(object)){
                return i;
            }
        }
        System.out.println("Sorry, element not found");
        return -1;
    }

    @Override
    public int lastIndexOf(E e) {
        for (int i = filled-1;  i >= 0; i--){
            if (data[i].equals(e)){
                return i;
            }
        }
        System.out.println("Element not found");
        return -1;
    }

    @Override
    public E remove(int index) {
        if (index >= filled || index < 0){
            System.out.println("Sorry, index out of bound");
            return null;
        }
        E temp = data[index];

        filled--;
        for (int i = filled; i > index ; i--){
            data[i-1] = data[i];
        }
        return temp;
    }

    @Override
    public E set(int index, E e) {
        if (index >= filled || index < 0){
            System.out.println("Sorry, index out of bound");
            return null;
        }

        E temp = data[index];
        data[index] = e;
        return temp;
    }

    // ====================================================================================== Collection  default dummy methods =============================================================

    @Override
    public int size() {
        return filled;
    }

    @Override
    public boolean isEmpty() {
        return filled == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean add(E e) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {

    }
}
