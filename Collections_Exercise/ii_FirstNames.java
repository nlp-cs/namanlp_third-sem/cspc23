import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ii_FirstNames {
    public static void main(String[] args) {
        int choice = 0;

        Set<String> myDataSet = new HashSet<>();

        Scanner scannedInput  = new Scanner(System.in);
        while (choice!=4){
            System.out.println("Enter choice, \n1->Enter New Name\n2->Search A name\n3->Print the data set\n4->Exit");
            choice = scannedInput.nextInt();
            scannedInput.nextLine(); // To skip line

            switch (choice){
                case 1: myDataSet.add(scannedInput.nextLine()); break;

                case 2 :
                    if (myDataSet.contains(scannedInput.nextLine()))
                        System.out.println("Yes, name is in the set");
                    else
                        System.out.println("No, name is not in the set");
                    break;

                case 3 : System.out.println("The set is : " + myDataSet); break;
                case 4:  System.out.println("Have a good day !"); break;

                default:
                    System.out.println("Sorry, wrong choice. Try again");
            }
        }
    }
}
