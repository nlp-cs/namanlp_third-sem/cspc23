public class Inheritance extends importingPackage {

    Inheritance(){
        staticData = 100;
        varData = 100;
    }
    Inheritance(int a){
        staticData = a;
        varData = a;
    }

    public static void main(String[] args) {

        // This will print data as 0 because it is static and called without making any object
        System.out.println("Data in given class is : "  + staticData);
    }
}
