import myPackage.hello;

public class importingPackage {

    public static int staticData;
    protected int varData;
    public static void main(String[] args) {
        System.out.println("Hello world!");
        hello.printHello();

        // This will print 20, because static variable is changed for whole class
        // If we change static data of inherited class, static variable of parent class also changes
        Inheritance var = new Inheritance();
        Inheritance var2 = new Inheritance(20);
        System.out.println("This is Static data : " + staticData );
        System.out.println("This is Variable data : " + var.varData );
    }
}