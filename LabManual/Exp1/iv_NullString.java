class stringClass{
    String str;
}

public class iv_NullString {
    public static void main(String[] args){
        stringClass object1 = new stringClass();

        if (object1.str == null){
            System.out.println("Yes, it is initialized to null by default");
        }
        else {
            System.out.println("No, it is not initialized to null by default");
        }
    }
}
