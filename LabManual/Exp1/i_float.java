import java.util.Scanner;

class FloatNumber{
    float data = 1.32f;
}

public class i_float {

    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        FloatNumber num1 = new FloatNumber();
        FloatNumber alias = num1;
        System.out.println("Data of  object before changing : " + alias.data);

        System.out.println("Enter data : ");
        num1.data = scannedInput.nextFloat();
        System.out.println("Data of aliased object after changing original object : " + alias.data);
    }
}
