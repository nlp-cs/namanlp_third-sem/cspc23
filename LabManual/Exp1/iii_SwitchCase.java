import java.util.Scanner;

public class iii_SwitchCase {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        int choice = scannedInput.nextInt();

        switch (choice) {
            case 1 -> System.out.println(" Hello ");
            case 2 -> System.out.println(" Hello 2");
            case 3 -> System.out.println(" Hello 3");
            default -> System.out.println(" Default");
        }
    }
}
