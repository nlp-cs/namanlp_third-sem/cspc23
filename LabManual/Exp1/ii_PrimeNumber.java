import java.util.Scanner;

public class ii_PrimeNumber {

    public static void main(String[] args){
        System.out.println("Enter max range : ");
        Scanner scannedInput = new Scanner(System.in);

        int maxRange = scannedInput.nextInt();

        if (maxRange >= 2 && maxRange < Integer.MAX_VALUE){
            for (int i = 2; i <= maxRange; i++){
                boolean isPrime = true;
                int j = 2;
                while (j*j <= i){
                    if (i % j == 0){
                        isPrime = false;
                        break;
                    }
                    else {j++;}
                }

                if (isPrime){
                    System.out.print( i + "  ");
                }
            }
        }

        else {
            if (maxRange < 2){
                System.out.println("Sorry, range must be greater than 2");
            }else {
                System.out.println("Sorry, max range must be integer");
            }
        }

    }
}
