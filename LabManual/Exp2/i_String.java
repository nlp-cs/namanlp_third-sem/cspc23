/*1. Write a method that takes two String arguments and uses all the boolean comparisons to
        compare the two Strings and print the results. For the == and !=, also perform the equals( ) test.
        In main( ), call your method with some different String objects.*/

import java.util.Scanner;

public class i_String {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Enter First String : ");
        String firstString = scannedInput.nextLine().trim();

        System.out.println("Enter Second String : ");
        String secondString = scannedInput.nextLine().trim();

        System.out.println("\nComparing According to dictionary");
        if (firstString.equals(secondString)){
            System.out.println("Both Strings are equal");
        }
        else if (firstString.compareTo(secondString) < 0){
            System.out.println("First string comes first in dictionary");
        }
        else {
            System.out.println("Second string comes first in dictionary");
        }

        System.out.println("\nComparing According to Length");
        if (firstString.length() == secondString.length()){
            System.out.println("Both have same length !");
        }
        else if (firstString.length() > secondString.length()){
            System.out.println("First string is larger !");
        }
        if (firstString.length() < secondString.length()){
            System.out.println("Second string is larger !");
        }

        boolean flag = false;
        System.out.println("\nOther comparison operators");
        if (firstString.startsWith("n") || firstString.startsWith("N") || secondString.startsWith("n") || secondString.startsWith("N")){
            System.out.println("My name also starts with N !");
            flag = true;
        }
        if ( firstString.endsWith("to") || secondString.endsWith("to")){
            System.out.println("Tomato also ends with \"to\"");
            flag = true;
        }
        if ( firstString.toLowerCase().contains("hello") || secondString.toLowerCase().contains("hello")){
            System.out.println("Hello to you too");
            flag = true;
        }
        if (!flag){
            System.out.println("Sorry, nothing special in string !");
        }
    }
}
