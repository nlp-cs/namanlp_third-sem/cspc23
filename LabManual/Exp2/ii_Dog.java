public class ii_Dog {

    static class Dog{
        String name;
        String says;

        Dog(String n, String s){
            name = n;
            says = s;
        }

        void print(){
            System.out.println(name + " says " + says);
        }
    }

    public static void main(String[] args) {
        Dog dog1 = new Dog("spot", "Ruff!");
        Dog dog2 = new Dog("scruffy", "Wurf!");
        dog1.print();
        dog2.print();

        Dog dog3 = dog1;

        if (dog1 == dog3){
            System.out.println("Both dogs are equal reference");
        }
        if (dog1.equals(dog3)){
            System.out.println("Both dogs are equal");
        }

    }
}
