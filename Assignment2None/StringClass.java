import java.util.Scanner;

public class StringClass {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Input any string : ");
        String givenString = scannedInput.nextLine().trim();

        System.out.println("Length of string is " + givenString.length());
        for (int i = 0; i<givenString.length(); i++){
            if (Character.isWhitespace(givenString.charAt(i))){
                System.out.println("First whitespace of string occurs at "+ (i+1) + " position");
                break;
            } else if (i == givenString.length()-1) {
                System.out.println("String has no whitespaces!");
            }
        }

        System.out.println("Upper Case of given string is : " + givenString.toUpperCase());
        System.out.println("Lower Case of given string is : " + givenString.toLowerCase());

        if (givenString.contains("Hello")){
            System.out.println("Hello to you too");
            System.out.println("Hello appears at " + (givenString.indexOf("Hello") + 1));
        }

        System.out.println("Enter double : ");
        String doubleString = scannedInput.next();

        double givenDouble = Double.parseDouble(doubleString);
        System.out.println("Given double divided by 12 gives " + givenDouble/12.0 + " 😃");

    }
}
