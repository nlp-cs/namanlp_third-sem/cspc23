public class EmojiPattern {
    public static void main(String[] args){
        for (int i = 0; i < 20 ; i++){
            for (int j = 0; j < 20- i; j++){
                if ((j+i)%3==0){
                    System.out.print(" 😁");
                } else if ((j+i)%3 == 1) {
                    System.out.print(" 💀");
                }
                else {
                    System.out.print(" 👺");
                }
            }
            System.out.println();
        }
    }
}
