public class Unicode {
    public static void main(String[] args) {
        String str="java2blog";
        String char1 = getUnicodeCharacterOfChar(str.charAt(3));
        System.out.println("Unicode value of character at 4th position: "+char1);

        String char2 = getUnicodeCharacterOfChar('ऊ');
        System.out.println("Unicode value of character क: "+char2);
        char char3 = '®';
        System.out.println(String.format("Unicode character code: %d", (int) char3));
        char first1 = '\u002F';
        System.out.println(first1);
        first1 = '\u002F';
        System.out.println(first1);
        for(char c=0x0905;c<=2361;c++) System.out.print(" " +c);
        System.out.println();//U+0900 - U+097F i.e. 2309decimal starting to 2361
        for(char c=0x0C05;c<=0x0C7F;c++)System.out.print(" " +c);
        System.out.println();
    }

    private static String getUnicodeCharacterOfChar(char ch) {
        return String.format("\\u%04x", (int) ch);
    }
}
