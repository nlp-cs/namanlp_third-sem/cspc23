import java.util.Scanner;

public class CharacterClass {
    public static void main(String[] args){
        Scanner scannedInput = new Scanner(System.in);

        System.out.println("Input any character");
        char givenChar = scannedInput.next().charAt(0);

        System.out.println("Given First Character : " + givenChar);

        if (Character.isWhitespace(givenChar)){
            System.out.println("Given Character is a whitespace");
        }else if (Character.isDigit(givenChar)){
            System.out.println("Given Character is digit!");
        } else if (Character.isLetter(givenChar)) {

            if (Character.isLowerCase(givenChar)){
                System.out.println("Given Character is a lower case letter");
                System.out.println("Upper case of letter is : " + Character.toUpperCase(givenChar));
            }
            else {
                System.out.println("Given Character is an upper case letter");
                System.out.println("Lower case of letter is : " + Character.toLowerCase(givenChar));
            }
        }
        else {
            System.out.println("Given Character has unicode " + (int)givenChar);
        }
    }
}
