import java.util.*;

public class PracticeList {
    public static void main(String[] args) {
        List<String> myData = new ArrayList<>();

        System.out.print("Enter number of Lines : ");
        Scanner scannedInput = new Scanner(System.in);
        int numberOfInputs = scannedInput.nextInt();
        // To clear the current line
        scannedInput.nextLine();

        System.out.print("Enter data : ");
        for (int i = 0; i < numberOfInputs; i++){
            String temp = scannedInput.nextLine();
            myData.add(temp);
        }

        System.out.println("Data entered is : " + myData);
        myData.add("Done");
        Collections.sort(myData);

        myData.add(myData.toString());
        System.out.println("Array list after modification is : " + myData);

        System.out.println("Enter 1 more line");
        String temp = scannedInput.nextLine();

        myData.set(0, temp);
        myData.remove(myData.size()-1);
        System.out.println("New Array List is : " + myData);
    }
}
